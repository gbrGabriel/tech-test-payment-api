using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private static List<Venda> _listaVendas = new List<Venda>();

        [HttpPost("FinalizarVenda")]
        public IActionResult Gravar(Venda venda)
        {
            venda.Id = new Random().Next(1, 999);
            venda.CodigoStatus = 4;

            _listaVendas.Add(venda);

            return Ok(venda);
        }

        [HttpGet("BuscarVenda/{id}")]
        public IActionResult Buscar(int id)
        {
            var venda = _listaVendas.Where(x => x.Id == id)?.FirstOrDefault();

            if (venda == null) return NotFound();

            return Ok(venda);
        }

        [HttpGet("ListarVendas")]
        public IActionResult ListarVendas()
        {
            return Ok(_listaVendas);
        }

        [HttpPut("AtualizarStatusVenda/{id}&{status}")]
        public IActionResult Atualizar(int id, int status)
        {
            var venda = _listaVendas.Where(x => x.Id == id)?.FirstOrDefault();
            if (venda == null) return NotFound();

            ValidarStatusVenda(ref venda, status);

            return Ok(venda);
        }

        private Venda ValidarStatusVenda(ref Venda venda, int codigoStatus)
        {
            if (venda.CodigoStatus == 4 && codigoStatus == 0 || codigoStatus == 3)
            {
                venda.CodigoStatus = codigoStatus;
            }
            else if (venda.CodigoStatus == 0 && codigoStatus == 1 || codigoStatus == 3)
            {
                venda.CodigoStatus = codigoStatus;
            }
            else if (venda.CodigoStatus == 1 || codigoStatus == 2)
            {
                venda.CodigoStatus = codigoStatus;
            }
            else
            {
                throw new Exception("Não é possível alterar o status da venda.");
            }
            return venda;
        }

    }
}