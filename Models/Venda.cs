using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public string Vendedor { get; set; }
        public List<Item> Items { get; set; }
        public int CodigoStatus { get; set; }
        public string Status => retornaStatus();

        public string retornaStatus()
        {
            var Status = string.Empty;
            switch (CodigoStatus)
            {
                case 0: Status = "Pagamento Aprovado"; break;
                case 1: Status = "Enviado para transportadora"; break;
                case 2: Status = "Entregue"; break;
                case 3: Status = "Cancelada"; break;
                case 4: Status = "Aguardando Pagamento"; break;
            }
            return Status;
        }
    }


}